<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package vantage
 * @since vantage 1.0
 * @license GPL 2.0
 */

get_header(); ?>

<div id="primary" class="content-area">
	<div id="content" class="site-content" role="main">

		<article id="post-0" class="post error404 not-found">

			<div class="entry-main">

				<?php do_action('vantage_entry_main_top') ?>

				<header class="entry-header">
					<div id="container-header">
						<div class="texteContainer">
							<?php if( siteorigin_page_setting( 'page_title' ) ) : ?>
								<h1 class="entry-title"><?php echo apply_filters( 'vantage_404_title', __( "Cette page n'a pas pu être trouvée.", 'amc-theme' ) ); ?></h1>
							<?php endif; ?>
						</div>
					</div>
				</header><!-- .entry-header -->

				<div class="entry-content">
					<p><?php echo apply_filters( 'vantage_404_message', __( 'Il semble que rien n\'ait été trouvé à cette endroit. Essayer une recherche?', 'vantage' ) ); ?></p>

					<form method="get" class="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>" role="search">
						<input type="text" class="field" name="s" value="<?php echo esc_attr( get_search_query() ); ?>" placeholder="<?php esc_attr_e( 'Recherche', 'amc-theme' ); ?>"/>
						<input type="submit" class="btnEnvoi" id="boutonRecherche" value="&#xf002;"/>
					</form>
				</div><!-- .entry-content -->

				<?php do_action('vantage_entry_main_bottom') ?>

			</div><!-- .entry-main -->

		</article><!-- #post-0 .post .error404 .not-found -->

	</div><!-- #content .site-content -->
</div><!-- #primary .content-area -->

<?php get_footer(); ?>
