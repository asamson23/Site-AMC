	<?php
	/**
	 * The template for displaying Archive pages.
	 *
	 * Learn more: http://codex.wordpress.org/Template_Hierarchy
	 *
	 * @package vantage
	 * @since vantage 1.0
	 * @license GPL 2.0
	 */

	get_header(); ?>

	<section id="primary" class="content-area">
		<div id="content" class="site-content" role="main">

			<header class="page-header">
				<div id="container-header">
					<div class="texteContainer">
						<!--<h1 id="page-title"><?php if( siteorigin_page_setting( 'page_title' ) ) { echo vantage_get_archive_title(); } ?></h1>-->
						<h1 id="page-title"><?php echo __('Produits', 'amc-theme') ?></h1>
							<?php
							if ( is_category() ) {
								// show an optional category description
								$category_description = category_description();
								if ( ! empty( $category_description ) )
									echo apply_filters( 'vantage_category_archive_meta', '<div class="taxonomy-description">' . $category_description . '</div>' );

							}
							elseif ( is_tag() ) {
								// show an optional tag description
								$tag_description = tag_description();
								if ( ! empty( $tag_description ) )
									echo apply_filters( 'vantage_tag_archive_meta', '<div class="taxonomy-description">' . $tag_description . '</div>' );
							}
							else {
								$description = term_description();
								if ( ! empty( $description ) )
									echo apply_filters( 'vantage_taxonomy_archive_meta', '<div class="taxonomy-description">' . $description . '</div>' );
							}
							?>
					</div>
				</div>
			</header><!-- .page-header -->

		<?php /* Code repris de loop-grid.php */ ?>

		<?php if( have_posts() ) : $i = 0; ?>
		<div id="contenu">
			<!--Fonction désactivée à raison d'un bug avec les liens-->
			<aside id="categoriesProduits">
				<h2 class="categoriesProduits"><?php echo __('Catégorie de produit', 'amc-theme')?></h2>
				<p><?php wp_nav_menu(array(
					'theme_location' => 'menu-produits',
					'container_class' => '',
					'container' => 'p',
					'menu_class'=> 'menu-produits',
				)); ?></p>
			</aside>
			<div id="vantage-grid-loop" class="vantage-grid-loop grid-loop-columns-<?php echo siteorigin_setting('blog_grid_column_count') ?>">
				<?php while( have_posts() ): the_post(); $i++; ?>
					<article <?php post_class(array('grid-post')) ?>>

						<?php if( has_post_thumbnail() ) : ?>
							<a class="grid-thumbnail" href="<?php the_permalink() ?>">
								<?php the_post_thumbnail('thumbnail-produit') ?>
							</a>
						<?php endif; ?>

						<h3><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h3>
					</article>
					<?php if($i % siteorigin_setting( 'blog_grid_column_count' ) == 0) : ?><div class="clear"></div><?php endif; ?>
				<?php endwhile; ?>
			</div>
		</div>

		<?php vantage_content_nav( 'nav-below' ); ?>

	<?php endif; ?>

		</div><!-- #content .site-content -->
	</section><!-- #primary .content-area -->

	<?php get_sidebar(); ?>
	<?php get_footer(); ?>
