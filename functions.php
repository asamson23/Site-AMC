<?php
function amc_enqueue_styles() {

    $parent_style = 'vantage-style'; // This is 'twentyfifteen-style' for the Twenty Fifteen theme.

    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style ),
        wp_get_theme()->get('Version')
    );
}

function initialisation_theme_amc() {
/*Fonction qui permet d'ajouter les produits à des posts de type produits*/
    register_post_type( 'produits',
        array(
            'labels' => array(
                'name' => __('Produits'),
                'singular_name' => __('Produit'),
                'menu_name'          => __( 'Produits' ),
		        'name_admin_bar'     => __( 'Produit' ),
		        'add_new'            => __( 'Ajouter Produit' ),
		        'add_new_item'       => __( 'Ajouter nouveau Produit' ),
		        'new_item'           => __( 'Nouveau Produit' ),
		        'edit_item'          => __( 'Modifier le produit' ),
		        'view_item'          => __( 'Voir le produit' ),
                'all_items'          => __( 'Tous les produits' ),
                'search_items'       => __( 'Chercher les produits' ),
                'parent_item_colon'  => __( 'Produits parents:' ),
                'not_found'          => __( 'Pas de produit trouvé.' ),
                'not_found_in_trash' => __( 'Pas de produit trouvé dans la corbeille.' )
            ),
            'public' => true,
            'has_archive' =>true,
            'menu_position' => 20,
            'menu_icon' => 'dashicons-cart',
            'supports' => array('title', 'thumbnail', 'custom-fields', 'revisions', 'excerpt'),
            'taxonomies' => array('post_tag')
        )
    );
    register_taxonomy( 'type', 'produits', array( 'hierarchical' => true, 'label' => __('Type de produit'), 'query_var' => true, 'rewrite' => true ) );

    /*Fonctions d'enregistrement des divers menus au sein du style*/
    register_nav_menu( 'mobile-header-menu', __('Menu d\'entête mobile') );
    register_nav_menu( 'footer-menu', __('Menu footer') );
    register_nav_menu('menu-produits', __('Menu produits'));
}

function widgets_amc_init() {
    register_sidebar(array(
        'name' => 'Facebook Feed',
        'id' => 'facebook_widget'
    ));
    register_sidebar(array(
        'name' => 'Twitter Feed',
        'id' => 'twitter_widget'
    ));
    register_sidebar(array(
        'name' => 'Instagram Feed',
        'id' => 'instagram_widget'
    ));
    
}

/*Fonction de tri des produits dans les pages d'archives de produits*/
function autosort_produits($query) {
    if(is_archive()):
        $query->set('order', 'ASC');
        $query->set('orderby', 'title');
    endif;
};

/*Fonction de traduction du thème*/
/**Tous les textes sources doivent être rédigés en FRANÇAIS!!!**/
function amc_theme_setup() {
    load_child_theme_textdomain( 'amc-theme', get_stylesheet_directory() . '/languages' );
}

/*Fonction pour ajouter les scripts supplémentaires au thème*/
function scripts_supplementaires() {
    wp_enqueue_script('converter', get_stylesheet_directory_uri() . '/js/converter.min.js', array('jquery'), true, true);
    wp_enqueue_script('menu-mobile', get_stylesheet_directory_uri() . '/js/menu-toggle.min.js', array('jquery'), true, true);
}

/** https://gist.github.com/charleslouis/5924863 */
/**
 * [list_searcheable_acf list all the custom fields we want to include in our search query]
 * @return [array] [list of custom fields]
 */
function list_searcheable_acf(){
    $list_searcheable_acf = array("carte_adresse", "description", "specifications", "homologation", "formats", "numero_piece");
    return $list_searcheable_acf;
  }
  /**
   * [advanced_custom_search search that encompasses ACF/advanced custom fields and taxonomies and split expression before request]
   * @param  [query-part/string]      $where    [the initial "where" part of the search query]
   * @param  [object]                 $wp_query []
   * @return [query-part/string]      $where    [the "where" part of the search query as we customized]
   * see https://vzurczak.wordpress.com/2013/06/15/extend-the-default-wordpress-search/
   * credits to Vincent Zurczak for the base query structure/spliting tags section
   */
  function advanced_custom_search( $where, &$wp_query ) {
      global $wpdb;
   
      if ( empty( $where ))
          return $where;
   
      // get search expression
      $terms = $wp_query->query_vars[ 's' ];
      
      // explode search expression to get search terms
      $exploded = explode( ' ', $terms );
      if( $exploded === FALSE || count( $exploded ) == 0 )
          $exploded = array( 0 => $terms );
           
      // reset search in order to rebuilt it as we whish
      $where = '';
      
      // get searcheable_acf, a list of advanced custom fields you want to search content in
      $list_searcheable_acf = list_searcheable_acf();
      foreach( $exploded as $tag ) :
          $where .= " 
            AND (
              (amc_wp_posts.post_title LIKE '%$tag%')
              OR (amc_wp_posts.post_content LIKE '%$tag%')
              OR EXISTS (
                SELECT * FROM amc_wp_postmeta
                    WHERE post_id = amc_wp_posts.ID
                      AND (";
          foreach ($list_searcheable_acf as $searcheable_acf) :
            if ($searcheable_acf == $list_searcheable_acf[0]):
              $where .= " (meta_key LIKE '%" . $searcheable_acf . "%' AND meta_value LIKE '%$tag%') ";
            else :
              $where .= " OR (meta_key LIKE '%" . $searcheable_acf . "%' AND meta_value LIKE '%$tag%') ";
            endif;
          endforeach;
              $where .= ")
              )
              OR EXISTS (
                SELECT * FROM amc_wp_comments
                WHERE comment_post_ID = amc_wp_posts.ID
                  AND comment_content LIKE '%$tag%'
              )
              OR EXISTS (
                SELECT * FROM amc_wp_terms
                INNER JOIN amc_wp_term_taxonomy
                  ON amc_wp_term_taxonomy.term_id = amc_wp_terms.term_id
                INNER JOIN amc_wp_term_relationships
                  ON amc_wp_term_relationships.term_taxonomy_id = amc_wp_term_taxonomy.term_taxonomy_id
                WHERE (
                    taxonomy = 'post_tag'
                      OR taxonomy = 'category'          		
                      OR taxonomy = 'myCustomTax'
                    )
                    AND object_id = amc_wp_posts.ID
                    AND amc_wp_terms.name LIKE '%$tag%'
              )
          )";
      endforeach;
      return $where;
  }

add_action( 'wp_enqueue_scripts', 'amc_enqueue_styles' );
add_action('init', 'initialisation_theme_amc' );
add_action( 'after_setup_theme', 'amc_theme_setup');
add_action('widgets_init', 'widgets_amc_init');
add_action('wp_enqueue_scripts', 'scripts_supplementaires');
add_action('pre_get_posts', 'autosort_produits');

add_filter( 'posts_search', 'advanced_custom_search', 500, 2 );

add_image_size( 'thumbnail-produit', 200, 200, true );