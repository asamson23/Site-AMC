/**
 * Script qui se charge sur la page d'accueil afin de changer les span du slider en lien avec une apparence de boutons
 */

var converter = $(function configurer(event) {
    setTimeout(convertSpan);

    function convertSpan(e) {
        $('span.bouton-lien').each(function(i) {
            $(this).replaceWith("<a href='" + $(this).attr("href") + "' class='bouton-lien'>" + $(this).text() + "</a>");
        });
    }
});