/**
 * Cette fonction sert à faire fonctionner l'ouverture et la fermeture du menu mobile
 * This function purpose is to enable the opening and closing of the mobile menu
 */

var menu_toggle = $(function configurer(event) {

    var toggle = "off";
    $('#searchNavMobile').addClass("menuHidden");
    $('#menuBtn').removeClass("menuHidden");
    $('#menuBtn').on("click", activerDesactiverMenu);

    function activerDesactiverMenu() {
        // if ($('#searchNavMobile').hasClass("menuHidden")) {
        //     $('#searchNavMobile').removeClass("menuHidden").addClass("menuActif");
        //     // $('#searchNavMobile').show();
        //     $('#searchNavMobile').hide().slideDown(500);
        //     $('#boutonMenu').removeClass("fa-bars").addClass("fa-times");
        // } else if ($('#searchNavMobile').hasClass("menuActif")) {
        //     $('#searchNavMobile').removeClass("menuActif").addClass("menuHidden");
        //     // $('#searchNavMobile').hide();
        //     $('#searchNavMobile').slideUp(500);
        //     $('#boutonMenu').removeClass("fa-times").addClass("fa-bars");
        // }
        if (toggle == 'off') {
            $('#searchNavMobile').hide().removeClass('menuHidden').slideDown().addClass('menuActif');
            $('#boutonMenu').removeClass("fa-bars").addClass("fa-times");
            toggle = 'on';
        } else if (toggle == 'on') {
            $('#searchNavMobile').show().removeClass('menuActif').slideUp();
            $('#searchNavMobike').addClass('menuHidden');
            $('#boutonMenu').removeClass("fa-times").addClass("fa-bars");
            toggle = 'off';
        }
    }
});