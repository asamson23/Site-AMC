<?php
/**
 * The template part for displaying a message that posts cannot be found.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package vantage
 * @since vantage 1.0
 * @license GPL 2.0
 */
?>

<article id="post-0" class="post no-results not-found">
	<header class="entry-header">
		<div id="container-header">
			<div class="texteContainer">
				<?php if ( siteorigin_page_setting( 'page_title' ) ) : ?>
					<h1 class="entry-title"><?php _e( 'Aucun résultat', 'amc-theme' ); ?></h1>
				<?php endif; ?>
			</div>
		</div>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php if ( is_home() && current_user_can( 'publish_posts' ) ) : ?>

			<p><?php printf( __( 'Prêt à publier votre première publication? <a href="%1$s">Commencez ici </a>.', 'amc-theme' ), esc_url( admin_url( 'post-new.php' ) ) ); ?></p>

		<?php elseif ( is_search() ) : ?>

			<p><?php _e( 'Désolé, mais rien ne correspond à vos termes de recherche. Veuillez réessayer avec d\'autres mots-clés.', 'amc-theme' ); ?></p>
			<?php get_search_form(); ?>

		<?php else : ?>

			<p><?php _e( 'Il semble que nous ne puissions pas trouver ce que vous cherchez. Peut-être que la recherche peut aider.', 'amc-theme' ); ?></p>
			<?php get_search_form(); ?>

		<?php endif; ?>
	</div><!-- .entry-content -->
</article><!-- #post-0 .post .no-results .not-found -->
