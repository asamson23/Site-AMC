<?php
    /**
    Template name: Page contact
    **/

get_header(); ?>
<!--Fin du header-->
<div id="primary" class="content-area">
	<div id="content" class="site-content" role="main">

		<?php while ( have_posts() ) : the_post(); ?>

        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        
            <div class="entry-main">
        
                <?php do_action('vantage_entry_main_top') ?>
        
                <?php if ( ( the_title( '', '', false ) && siteorigin_page_setting( 'page_title' ) ) || ( has_post_thumbnail() && siteorigin_page_setting( 'featured_image' ) ) ) : ?>
                    <header class="entry-header">
                        <div id="container-header">
                            <div class="texteContainer">
                                <?php if ( has_post_thumbnail() && siteorigin_page_setting( 'featured_image' ) ) : ?>
                                    <div class="entry-thumbnail"><?php vantage_entry_thumbnail(); ?></div>
                                <?php endif; ?>
                                <?php if ( the_title( '', '', false ) && siteorigin_page_setting( 'page_title' ) ) : ?>
                                    <h1 class="entry-title"><?php the_title(); ?></h1>
                                <?php endif; ?>
                            </div>
                        </div>
                    </header><!-- .entry-header -->
                <?php endif; ?>
                <div class="entry-content">
                    <div class="fiftyPcGauche">
                        <?php the_content(); ?>
                    </div>
                    <div class="fiftyPcDroite">
                        <?php if (get_field('carte_adresse')): ?>
                            <?php the_field('carte_adresse'); ?><br/>
                        <?php endif; ?>
                    </div>
                    <!-- <?php wp_link_pages( array( 'before' => '<div class="page-links">' . __( 'Pages:', 'vantage' ), 'after' => '</div>' ) ); ?> -->
                </div><!-- .entry-content -->
        
                <?php do_action('vantage_entry_main_bottom') ?>
            </div>
        
        </article><!-- #post-<?php the_ID(); ?> -->

		<?php endwhile; // end of the loop. ?>

	</div><!-- #content .site-content -->
</div><!-- #primary .content-area -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>