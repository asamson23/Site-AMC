<?php
/**
 * Part Name: Footer Personnalisé
 */
?>
<footer id="colophon" class="site-footer" role="contentinfo">

	<?php if( ! siteorigin_page_setting( 'hide_footer_widgets', false ) ) : ?>
		<div id="footer-widgets" class="full-container">
			<?php dynamic_sidebar( 'sidebar-footer' ) ?>
            <?php wp_nav_menu(array(
				'theme_location' => 'footer-menu',
				'containter_class' => '',
				'container' => 'nav',
				'menu_class'=> 'menu-footer',
			)); ?>
			<!-- Menu Mobile -->
			<?php wp_nav_menu(array(
				'theme_location' => 'mobile-header-menu',
				'containter_class' => '',
				'container' => 'nav',
				'menu_class'=> 'menu-mobile',
				'menu_id' => 'mobile-footer'
			)); ?>
			<ul id="rSociaux">
				<li class="social-icon-liste"><a href="https://www.facebook.com/AutoMotoCanada" target="_blank" class="facebook-icon social-icon" title="Facebook"></a></li>
				<li class="social-icon-liste"><a href="https://twitter.com/AMCOfficiel" target="_blank" class="twitter-icon social-icon" title="Twitter"></a></li>
				<li class="social-icon-liste"><a href="https://instagram.com/automotocanada/" target="_blank" class="instagram-icon social-icon" title="Instagram"></a></li>
			</ul>
		</div><!-- #footer-widgets -->
	<?php endif; ?>

	<?php $site_info_text = apply_filters('vantage_site_info', siteorigin_setting('general_site_info_text') ); if( !empty($site_info_text) ) : ?>
		<div id="site-info">
			<?php echo wp_kses_post($site_info_text) ?>
		</div><!-- #site-info -->
	<?php endif; ?>

	<?php echo apply_filters( 'vantage_footer_attribution', '<div id="theme-attribution">' . sprintf( __('Un thème <a href="%s">SiteOrigin</a>', 'amc-theme'), 'https://siteorigin.com') . '</div>' ) ?>

</footer><!-- #colophon .site-footer -->
