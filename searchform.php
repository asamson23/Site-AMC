<?php
/**
 * The template for displaying search forms in vantage
 * Formulaire personnalisé pour afficher les boutons de recherches. Un bouton pour la version mobile et un bouton pour la version de table
 *
 * @package vantage
 * @since vantage 1.0
 * @license GPL 2.0
 */
?>

<form method="get" class="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>" role="search">
	<input type="text" class="field" name="s" value="<?php echo esc_attr( get_search_query() ); ?>" placeholder="<?php esc_attr_e( 'Recherche', 'amc-theme' ); ?>"/>
	<input type="submit" class="btnEnvoi" id="boutonRecherche" value="&#xf002;"/>
	<!--<input type="submit" class="btnEnvoi" id="boutonRechercheTexte" value="<?php esc_attr_e( 'Rechercher', 'amc-theme' ); ?>"/>-->
	<!--<button type="submit" class="btnEnvoi"><i class="fa fa-search" aria-hidden="true"></i> Rechercher</button>-->
</form>
