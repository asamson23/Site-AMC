<?php
/**
 * The Template for displaying all single posts of produits.
 *
 * @package vantage
 * @since vantage 1.0
 * @license GPL 2.0
 */

get_header(); ?>

<div id="primary" class="content-area">
	<div id="content" class="site-content" role="main">

	<?php while ( have_posts() ) : the_post(); ?>

		<article id="post-<?php the_ID(); ?>" <?php post_class('post'); ?>>

	<div class="entry-main">

		<?php do_action('vantage_entry_main_top') ?>

		<?php if ( ( the_title( '', '', false ) && siteorigin_page_setting( 'page_title' ) ) || ( has_post_thumbnail() && siteorigin_setting('blog_featured_image') ) || ( siteorigin_setting( 'blog_post_metadata' ) && get_post_type() == 'post' ) ) : ?>
			<header class="entry-header">

				<div id="container-header">
						<div class="texteContainer">
							<?php if( has_post_thumbnail() && siteorigin_setting('blog_featured_image') ): ?>
								<!-- <div class="entry-thumbnail"><?php vantage_entry_thumbnail(); ?></div> -->
							<?php endif; ?>

							<?php if ( the_title( '', '', false ) && siteorigin_page_setting( 'page_title' ) ) : ?>
								<h1 class="entry-title"><?php the_title(); ?></h1>
							<?php endif; ?>

							<?php if ( siteorigin_setting( 'blog_post_metadata' ) && get_post_type() == 'post' ) : ?>
								<div class="entry-meta">
									<?php vantage_posted_on(); ?>
								</div><!-- .entry-meta -->
							<?php endif; ?>
						</div>
					</div>
			</header><!-- .entry-header -->
		<?php endif; ?>

		<div class="entry-content">
			<div class="contentDTGauche">
			<?php the_content(); ?>
            <?php if(get_field('description')): ?>
                <?php echo '<h2>' . __('Description', 'amc-theme') . '</h2>'; ?>
                <?php the_field('description'); ?>
            <?php endif; ?>

            <?php if(get_field('specifications')): ?>
                <?php echo '<h2>' . __('Spécification', 'amc-theme') . '</h2>'; ?>
                <?php the_field('specifications'); ?>
            <?php endif; ?>

            <?php if(get_field('homologation')): ?>
                <?php echo '<h2>' . __('Homologation', 'amc-theme') . '</h2>'; ?>
                <?php the_field('homologation'); ?>
            <?php endif; ?>

            <?php if(get_field('formats')): ?>
                <?php echo '<h2>' . __('Formats', 'amc-theme') . '</h2>'; ?>
                <p><?php the_field('formats'); ?></p>
            <?php endif; ?>

            <?php if(get_field('numero_piece')): ?>
                <?php echo '<h2>' . __('Numéro de la pièce', 'amc-theme') . '</h2>'; ?>
                <?php the_field('numero_piece'); ?>
			<?php endif; ?>
			</div>
			<div class="contentUTDroite">
			<?php if(get_field('image_produit')): ?>
                <?php $image = get_field('image_produit');
                $size = 'medium';
                // $size = 'thumbnail';
                $imageLogo = $image['sizes'][$size];
                $width = $image['sizes'][ $size . '-width' ];
                $height = $image['sizes'][ $size . '-height' ];
                $url = $image['url'];
                $alt = $image['alt'];
                if (!empty($image)): ?>
                    <img class="imgProduit" src="<?php echo $imageLogo; ?>" alt="<?php echo $alt; ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" />
                <?php endif; ?>
            <?php endif; ?>
			</div>

			<?php wp_link_pages( array( 'before' => '<div class="page-links">' . __( 'Pages:', 'vantage' ), 'after' => '</div>' ) ); ?>
		</div><!-- .entry-content -->

		<?php if( vantage_get_post_categories() && ! is_singular( 'jetpack-testimonial' ) ) : ?>
			<div class="entry-categories">
				<?php echo vantage_get_post_categories() ?>
			</div>
		<?php endif; ?>

		<?php if ( is_singular() && siteorigin_setting( 'blog_author_box' ) ) vantage_author_box( $post ); ?>

		<?php do_action('vantage_entry_main_bottom') ?>

	</div>

</article><!-- #post-<?php the_ID(); ?> -->	

		<!--<?php if ( siteorigin_setting( 'navigation_post_nav' ) ) vantage_content_nav( 'nav-below' ); ?>

		<?php if ( comments_open() || '0' != get_comments_number() ) : ?>
			<?php comments_template( '', true ); ?>
		<?php endif; ?>

	<?php endwhile; // end of the loop. ?>

	</div><!-- #content .site-content -->
</div><!-- #primary .content-area -->

<?php get_sidebar(); ?>

<?php get_footer(); ?>